// Core
import React, { useEffect } from "react";

// Internal
import Page from "components/templates/Page";
import Card from "components/molecules/Card";
import iDashboardItem from "interfaces/iDashboardItem";

export default function Home() {
  // Properties
  const data: iDashboardItem[] = require("./dashboard.json");
  const adminModules = data.filter((item) => item.type === "admin");
  const taModules = data.filter((item) => item.type === "ta");

  // Constructor
  useEffect(() => {
    document.title = `SDA Admin`;
  }, []);

  return (
    <Page id="dashboard">
      <h1>
        SDA Admin <br />
        Platform
      </h1>

      <h2>Admin task</h2>
      <div className="grid-container">
        {adminModules.map((item) => {
          return <Card key={item.id} props={item} />;
        })}
      </div>

      <h2>Teaching assistant tasks</h2>
      <div className="grid-container">
        {taModules.map((item) => {
          return <Card key={item.id} props={item} />;
        })}
      </div>
    </Page>
  );
}
