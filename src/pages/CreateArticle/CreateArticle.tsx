// External dependencies
import React, { useState } from "react";
import { useParams } from "react-router-dom";

// Internal dependencies
import iArticle from "interfaces/iArticle";
import Page from "components/templates/Page";
import ButtonTextIcon from "components/atoms/ButtonTextIcon";
import Model from "./Model";

export default function CreateArticle() {
  const { id } = useParams();
  const [article, setArticle] = useState({ id: id } as iArticle);
  const model = new Model(id);

  function changeTitle(event: any) {
    setArticle({ ...article, name: event.target.value });
  }
  
  function changeContent(event: any) {
    setArticle({ ...article, content: event.target.value });
  }

  function onPublish() {
    if (article.name === undefined || article.name === "") {
      alert("The title cannot be empty");
      return;
    }
    if (article.content === undefined || article.content === "") {
      alert("The content cannot be empty");
      return;
    }

    alert(`Preparing to publish ${article.name}`);
    model.updateItem(article);
  }

  function onGoBack() {
    const message =
      "Are you sure you want to go back, any changes made withouth savig will be lost";
    if (window.confirm(message)) {
      alert("going back");
    } else {
      alert("canceled");
    }
  }

  return (
    <Page id="create-article">
      <h1>Course name</h1>

      <input
        type="text"
        placeholder="Write your title here"
        value={article.name}
        onChange={changeTitle}
      />

      <textarea className="text-editor" onChange={changeContent}></textarea>
      <br />
      <button onClick={onPublish}>Publish</button>

      <footer className="footer">
        <hr />
        <ButtonTextIcon icon="arrow-back" onClick={onGoBack}>
          Back to course editor
        </ButtonTextIcon>
      </footer>
    </Page>
  );
}
