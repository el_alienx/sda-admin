import firebase from "utils/firebase";
import iArticle from "interfaces/iArticle";

export default class Model {
  // Properties
  private readonly COLLECTION = "articles";
  private readonly _database = firebase.firestore();
  private _articleId = "";

  // Contructor
  constructor(articleId: string) {
    this._articleId = articleId;
  }

  // Methods
  private getQuery() {
    return this._database.collection(this.COLLECTION);
  }

  // This will automatically create or update the article
  public updateItem(item: iArticle) {
    console.log(item);

    return this.getQuery().doc(item.id).set(item);
  }
}
