// Core
import React from "react";
import { Link } from "react-router-dom";

export default function CourseMenu() {
  return (
    <div id="course-creator" className="container">
      <h1>Module Creator</h1>
      <h2>Learning modules</h2>
      <ol>
        <li>
          <Link to="course/programming-foundations">
            Programming Foundations
          </Link>
        </li>
        <li>
          <Link to="course/software-engineering">Software Engineering</Link>
        </li>
        <li>
          <Link to="course/programming-in-depth">Programming in Depth</Link>
        </li>
        <li>
          <Link to="course/web-development">Web Development</Link>
        </li>
      </ol>

      <h2>Evaluation modules</h2>
      <ol>
        <li>
          <Link to="course/individual-project">Individual Project</Link>
        </li>
        <li>
          <Link to="course/group-project">Group Project</Link>
        </li>
      </ol>
    </div>
  );
}
