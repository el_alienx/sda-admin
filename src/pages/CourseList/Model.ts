import firebase from "utils/firebase";
import iCourse from "interfaces/iCourse";

export default class Model {
  // Properties
  private readonly COLLECTION = "courses";
  private readonly _database = firebase.firestore();

  // Methods
  private getQuery() {
    return this._database.collection(this.COLLECTION);
  }

  public async fetchData(): Promise<Array<iCourse>> {
    const query = await this.getQuery().get();
    const collectionWithIds = query.docs.map(
      (doc) => ({ ...doc.data(), id: doc.id } as iCourse)
    );

    return collectionWithIds;
  }

  public createItem(newItem: iCourse) {
    return this.getQuery().add(newItem);
  }

  public async updateItem(newItem: iCourse) {
    return this.getQuery().doc(newItem.id).set(newItem);
  }

  public async deleteItem(id: string) {
    return this.getQuery().doc(id).delete();
  }
}
