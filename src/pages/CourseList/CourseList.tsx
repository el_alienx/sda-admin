// Core
import React, { useState, useEffect, useCallback } from "react";
import { Link } from "react-router-dom";
import Model from "./Model";
import iCourse from "interfaces/iCourse";

// Components
import Page from "components/templates/Page";

export default function CourseList() {
  // Properties
  const [data, setData] = useState(Array<iCourse>());

  // Methods
  const fetchData = useCallback(async () => {
    const model = new Model();
    const data = await model.fetchData();
    data.sort((a, b) => a.priority - b.priority);

    setData(data);
  }, []);

  // Constructor
  useEffect(() => {
    document.title = `SDA Course list`;
    fetchData();
  }, [fetchData]);

  return (
    <Page id="course-list">
      <h1>Module Creator</h1>

      <ul>
        {data.map((item, index) => (
          <li key={index}>
            <Link to={`course/${item.id}`}>{item.name}</Link>
          </li>
        ))}
      </ul>

      <footer className="footer">
        <hr />
        <Link className="button" to="/">
          Back to home
        </Link>
      </footer>
    </Page>
  );
}
