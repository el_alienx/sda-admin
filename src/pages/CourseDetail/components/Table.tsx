import React, { FC, ReactNode } from "react";
import "./TableStyle.sass";

interface Props {
  children: ReactNode;
}

const Table: FC<Props> = ({ children }) => {
  return (
    <table>
      <thead>
        <tr>
          <th>Order</th>
          <th>Name</th>
          <th>Icon</th>
          <th>Link</th>
          <th>Unlock date</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>{children}</tbody>
    </table>
  );
};

export default Table;
