// Core
import React, { useState, FC } from "react";
import iFile from "../../../interfaces/iFile";

// Components
import ButtonTextIcon from "components/atoms/ButtonTextIcon";

// Style
import "./RowStyle.sass";

interface Props {
  item: iFile;
  itemUpdate: Function;
  itemDelete: Function;
}

const Row: FC<Props> = ({ item, itemUpdate, itemDelete }) => {
  // Properties
  const [newItem, setNewItem] = useState(item);

  // Methods
  function handleChange(key: string, value: string) {
    setNewItem({ ...newItem, [key]: value });
  }

  function onSave() {
    itemUpdate(newItem);
  }

  return (
    <tr>
      <td>
        <input
          type="number"
          name="priority"
          defaultValue={newItem.priority}
          onChange={(event) => handleChange("priority", event.target.value)}
        />
      </td>
      <td>
        <input
          type="text"
          name="name"
          placeholder="File name"
          defaultValue={newItem.name}
          onChange={(event) => handleChange("name", event.target.value)}
        />
      </td>
      <td>
        <input
          type="text"
          name="icon"
          placeholder="Type a fontawesome.com icon"
          defaultValue={newItem.icon}
          onChange={(event) => handleChange("icon", event.target.value)}
        />
      </td>
      <td>
        <input
          type="text"
          name="link"
          placeholder="Paste link here"
          defaultValue={newItem.link}
          onChange={(event) => handleChange("link", event.target.value)}
        />
      </td>
      <td>
        <input
          type="text"
          name="date"
          placeholder="Example: Aug 21"
          defaultValue={newItem.date}
          onChange={(event) => handleChange("date", event.target.value)}
        />
      </td>
      <td>
        <ButtonTextIcon icon="check" onClick={onSave}>
          Save
        </ButtonTextIcon>
        <ButtonTextIcon icon="trash-alt" onClick={() => itemDelete(item.id)}>
          Delete
        </ButtonTextIcon>
      </td>
    </tr>
  );
};

export default Row;
