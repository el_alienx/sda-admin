import firebase from "utils/firebase";
import iFile from "interfaces/iFile";

export default class Model {
  // Properties
  private readonly COLLECTION_1 = "courses";
  private readonly COLLECTION_2 = "content";
  private readonly _database = firebase.firestore();
  private _course = "";

  // Contructor
  constructor(course: string) {
    this._course = course;
  }

  // Methods
  private getQuery() {
    return this._database
      .collection(this.COLLECTION_1)
      .doc(this._course)
      .collection(this.COLLECTION_2);
  }

  public async fetchData(): Promise<Array<iFile>> {
    const query = await this.getQuery().get();
    const collectionWithIds = query.docs.map(
      (doc) => ({ ...doc.data(), id: doc.id } as iFile)
    );

    return collectionWithIds;
  }

  // Refactor this methods are abstracted enought to be called from firebase.ts
  // Refactor, Warning: This has firebase code in the Reacr component
  public createItem(item: iFile) {
    return this.getQuery().add(item);
  }

  public async updateItem(item: iFile) {
    return this.getQuery().doc(item.id).set(item);
  }

  public async deleteItem(id: string) {
    return this.getQuery().doc(id).delete();
  }
}
