// Core
import React, { useEffect, useState, useCallback } from "react";
import { Link, useParams, useHistory } from "react-router-dom";
import Model from "./Model";
import iFile from "../../interfaces/iFile";

// Components
import Page from "components/templates/Page";
import Row from "./components/Row";
import Table from "./components/Table";
import ButtonTextIcon from "components/atoms/ButtonTextIcon";

// Style
import "./Style.sass";

export default function CourseDetail() {
  // Properties
  const { fileName } = useParams();
  const model = new Model(fileName);
  const history = useHistory();
  const [data, setData] = useState(Array<iFile>());
  const content = [
    {
      title: "Documents",
      type: "document",
    },
    {
      title: "Slides",
      type: "slide",
    },
    {
      title: "Videos",
      type: "video",
    },
    {
      title: "Worlshops",
      type: "workshop",
    },
    {
      title: "Coding exercises",
      type: "coding",
    },
    {
      title: "Evaluation",
      type: "evaluation",
    },
  ];

  // Methods
  const fetchData = useCallback(async () => {
    const data = await model.fetchData();
    data.sort((a, b) => a.priority - b.priority);

    setData(data);
  }, [model.fetchData]);

  function onCreateExternalFile(type: string) {
    const newExternalItem = {
      id: "",
      type: type,
      name: "",
      link: "",
      priority: 0,
      icon: "file-alt",
      date: "",
    };

    // Refactor, Warning: This has firebase code in the Reacr component
    model
      .createItem(newExternalItem)
      .then((doc) => setData([...data, { ...newExternalItem, id: doc.id }]))
      .catch((error) => console.error("Error adding file: ", error));
  }

  function onCreateInternalFile(type: string) {
    let newInternalItem = {
      id: "",
      type: type,
      name: "",
      link: "article/",
      priority: 0,
      icon: "file-alt",
      date: "",
    };

    model.createItem(newInternalItem).then((doc) => {
      newInternalItem.id = doc.id;
      newInternalItem.link = `/article/${doc.id}`;

      model
        .updateItem(newInternalItem)
        .then(() => history.push(`/create-article/${newInternalItem.id}`));
    });
  }

  function onUpdate(item: iFile) {
    model
      .updateItem(item)
      .then(() => console.log("updated!"))
      .catch((error) => console.error("Error updating file: ", error));
  }

  function onDelete(id: string) {
    model
      .deleteItem(id)
      .then(() => window.location.reload())
      .catch((error) => console.error("Error deleting file: ", error));
  }

  // Constructor
  useEffect(() => {
    document.title = `SDA Edit course`;
    fetchData();
  }, [fetchData]);

  // Components
  const Sections = content.map((item, index) => {
    const filteredFiles = data.filter(
      (filteredItem) => filteredItem.type === item.type
    );

    const EmptyState = <p>There is not files yet.</p>;
    const TableSystem = (
      <Table>
        {filteredFiles.map((item, index) => (
          <Row
            key={index}
            item={item}
            itemUpdate={onUpdate}
            itemDelete={onDelete}
          />
        ))}
      </Table>
    );

    return (
      <section key={index}>
        <h3>{item.title}</h3>
        {filteredFiles.length === 0 ? EmptyState : TableSystem}
        <ButtonTextIcon
          icon="plus"
          onClick={() => onCreateExternalFile(item.type)}
        >
          Add external file
        </ButtonTextIcon>
        <ButtonTextIcon
          icon="plus"
          onClick={() => onCreateInternalFile(item.type)}
        >
          Create article
        </ButtonTextIcon>
      </section>
    );
  });

  return (
    <Page id="course">
      <h1>Edit course</h1>

      {Sections}

      <footer className="footer">
        <hr />
        <Link className="button" to="/course-list">
          Back to course
        </Link>
      </footer>
    </Page>
  );
}
