// Core
import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function ItemVideo({ className, name, duration, onChange, id }) {
  return (
    <button className={`item-video ${className}`} onClick={() => onChange(id)}>
      <span className="icon">
        <FontAwesomeIcon icon="play" />
      </span>
      <span className="label">
        {id}. {name}
      </span>
      <span className="duration">{duration}</span>
    </button>
  );
}
