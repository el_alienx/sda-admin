// Core
import React from "react";
import { Link } from "react-router-dom";

export default function Card({ props }) {
  const { fileName, date, name } = props;
  const image = require(`assets/images/illustrations/${fileName}.png`);
  const route = `/${fileName}`;

  const Item = (
    <React.Fragment>
      <div className="image-container">
        <img className="image" src={image} alt={`${name} course cover`} />
      </div>
      <div className="label">{name}</div>
    </React.Fragment>
  );

  const ActiveItem = (
    <Link className="card" to={route}>
      {Item}
    </Link>
  );
  const InactiveItem = <div className="card dissabled">{Item}</div>;

  return date ? InactiveItem : ActiveItem;
}
