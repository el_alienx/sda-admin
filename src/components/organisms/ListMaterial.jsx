// Core
import React from "react";

// Components
import ItemMaterial from "components/molecules/ItemMaterial";
import Anchor from "components/atoms/Anchor";

export default function ListMaterial({ color, list, title }) {
  // This component does not render if the list is empty
  if (list.length === 0) return null;

  const Items = list.map((item) => {
    return (
      <Anchor
        className="item-material"
        href={item.link}
        isDisabled={item.date}
        key={item.id}
      >
        <ItemMaterial color={color} prop={item} />
      </Anchor>
    );
  });

  return (
    <section className="list-material">
      <h3>{title}</h3>
      <div className="grid-container">{Items}</div>
    </section>
  );
}
