// Core
import React from "react";
import { NavLink, Link } from "react-router-dom";

// Components
import ItemNavigation from "../molecules/ItemNavigation";
import Anchor from "../atoms/Anchor";

// Media
import Logo from "assets/images/logo.svg";
import Avatar from "../atoms/Avatar";

export default function NavigationBar() {
  const calendarURL =
    "https://docs.google.com/spreadsheets/d/1Xfe25FXe2ZnQC6klUHwwYBUYy31P9x71jufGQp-JIeU";
  const chatURL = "https://app.slack.com/client/TT8QR5H1D/CSU5LMSJE";

  return (
    <nav className="navigation-bar">
      {/* Logo */}
      <Link to="/" className="logo">
        <img src={Logo} alt="An circle with the letters S, D, and A inside" />
      </Link>

      <hr />

      {/* Options */}
      <NavLink
        className="item-navigation"
        activeClassName="selected"
        exact
        to="/"
      >
        <ItemNavigation icon="book-open" label="Courses" />
      </NavLink>
      <Anchor className="item-navigation" href={calendarURL}>
        <ItemNavigation icon="calendar-alt" label="Calendar" />
      </Anchor>
      <Anchor className="item-navigation" href={chatURL}>
        <ItemNavigation icon="comment-alt" label="Chat" />
      </Anchor>

      {/* User avatar */}
      <NavLink
        className="item-navigation profile"
        activeClassName="selected"
        to="/profile"
      >
        <ItemNavigation icon="user" label="Profile" />
        <Avatar />
      </NavLink>
    </nav>
  );
}
