import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "utils/fontawesome";

export default function ButtonTextIcon({ icon, children, onClick }) {
  return (
    <button className="button-text" onClick={onClick}>
      <span className="icon">
        <FontAwesomeIcon icon={icon} />
      </span>
      {children}
    </button>
  );
}
