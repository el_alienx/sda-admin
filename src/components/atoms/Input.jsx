// Core
import React from "react";

export default function Input({ props }) {
  const {
    autoComplete = false,
    autoFocus,
    id,
    label,
    placeholder,
    type,
  } = props;

  return (
    <fieldset className="field-set">
      <label htmlFor={id}>{label}</label>
      <input
        autoFocus={autoFocus}
        autoComplete={autoComplete}
        id={id}
        type={type}
        placeholder={placeholder}
        required
      />
    </fieldset>
  );
}
