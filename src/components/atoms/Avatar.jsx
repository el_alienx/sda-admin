// Core
import React from "react";

// Media
import TempAvatar from "assets/images/temp/avatar.jpg";

export default function Avatar() {
  return <img className="avatar" src={TempAvatar} alt="Your profile avatar" />;
}
