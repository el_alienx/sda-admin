// Database
import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/storage";

firebase.initializeApp({
  apiKey: "AIzaSyDeSVIm_d-p1K5iC5z7l1Bl0SsGVXc5qtU",
  authDomain: "sdaprojectest.firebaseapp.com",
  projectId: "sdaprojectest",
  storageBucket: "sdaprojectest.appspot.com",
});

export default firebase;
