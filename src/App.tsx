// Core
import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

// Components
import Home from "./pages/Home/Home";
import CourseList from "./pages/CourseList/CourseList";
import CourseDetail from "./pages/CourseDetail/CourseDetail";
import CreateArticle from "pages/CreateArticle/CreateArticle";

// Style
import "./styles/style.sass";

function App() {
  return (
    <Router>
      <div className="App">
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/course-list" component={CourseList} />
          <Route path="/course/:fileName" component={CourseDetail} />
          <Route path="/create-article/:id" component={CreateArticle} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
