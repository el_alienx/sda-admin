export default interface iArticle {
  id: string;
  name: string;
  course: string;
  content: string;
  link: string;
}
