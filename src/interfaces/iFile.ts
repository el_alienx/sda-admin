export default interface iFile {
  id: string;
  name: string;
  link: string;
  type: string;
  priority: number;
  icon: string;
  date: string;
}
