export default interface iCourse {
  id: string;
  image: string;
  name: string;
  description: string;
  priority: number;
  date: string;
  type: string; // Refactor: to be converted to Enum
}
